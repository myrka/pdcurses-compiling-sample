#include "TerminalCUI.h"

#define C_DEFAULT 0
#define C_ERROR 4
#define C_WARNING 5
#define C_GOOD 6

//erinevad v�ikesed aknad
WINDOW *status_window;
WINDOW *log_window[3];

//Terminali akna k�rgus ja laius karakterites
int width, height;
std::vector< std::vector<std::string> > logText(3);
std::vector<int> logRepeatingTexts(3);

//Statuse akna jaoks
std::string motor_port[3], motor_speed[3], motor_cmd[3];
std::string coilgun_port = "MISSING", coilgun_cmd = "-";
int dribbler_state = -1, ball_in_dribbler = -1, balls_found = -1;
int last_command_length = 20;

//m��rab akna sisu offseti
int posx = 0, posy = 0;

static int getStatusContentHeight(){
	return 10;
}

static int getStatusContentWidth(){
	return 30+last_command_length;
}

/* Loob uue 'akna' meie terminali sisse */
static WINDOW *create_newwin(int height, int width, int starty, int startx, std::string name){
	WINDOW *local_win;
	local_win = newwin(height, width, starty, startx);
	box(local_win, 0, 0);
	int x = width / 2 - name.size() / 2;
	wattron(local_win, A_UNDERLINE);
	mvwprintw(local_win, 0, x, name.c_str());
	wattroff(local_win, A_UNDERLINE);
	wrefresh(local_win);
	return local_win;
}

/* lihtlabane stringi asendusfunktsioon*/
static std::string replaceAll(std::string inputString, std::string find, std::string replace){
	size_t index = 0;
	while (true) {
		index = inputString.find(find, index);
		if (index == std::string::npos)
			break;
		inputString.replace(index++, find.length(), replace);
	}
	return inputString;
}

/* Prindib logi aknas oleva teksti �le */
static void updateLog(int logNum){
	int current_y = height / 2 - 2;
	int curx, cury;
	for (unsigned i = 0; i < logText[logNum].size() && current_y > 1; i++){
		std::string to_print = logText[logNum][logText[logNum].size() - 1 - i];

		if (i == 0){
			//esimese teksti puhul tuleb kordus ise teksti l�ppu lisada, kui vaja
			if (logRepeatingTexts[logNum] > 0)
				to_print += " [" + std::to_string(logRepeatingTexts[logNum] + 1) + "]";
		}

		int repeatingStart = to_print.find_last_of(" [");
		if (!(repeatingStart != std::string::npos  &&  to_print.back() == ']')){
			//peaks tegema l�pu v�rviliseks
			repeatingStart = 65535;
		}

		to_print = replaceAll(to_print, "\n", "\\n");
		int chars = to_print.size();
		int lines_needed = chars / (width / 2 - 2);
		wmove(log_window[logNum], current_y - lines_needed, 1);

		for (int i = 0; i < chars; i++){
			getyx(log_window[logNum], cury, curx);
			if (curx > width / 2 - 2){
				//liigutame kursori j�rgmisele reale
				wmove(log_window[logNum], cury + 1, 1);
			}

			//tekstisisesed stiili s�ttimised
			if ((to_print[i] == '\\'  &&  to_print[i + 1] == 'n') || (i > 0 && to_print[i - 1] == '\\'  &&  to_print[i] == 'n')){
				wattron(log_window[logNum], COLOR_PAIR(1));
			} else if (i >= repeatingStart){
				wattron(log_window[logNum], COLOR_PAIR(2));
			} else {
				wattroff(log_window[logNum], COLOR_PAIR(1));
				wattroff(log_window[logNum], COLOR_PAIR(2));
			}
			wprintw(log_window[logNum], "%c", to_print[i]);
		}
		getyx(log_window[logNum], cury, curx);
		while (curx < width / 2 - 2){
			wprintw(log_window[logNum], " ");
			getyx(log_window[logNum], cury, curx);
		}
		current_y -= lines_needed + 1;
	}
	wrefresh(log_window[logNum]);
}

static void writeTextWithColor(std::string text, WINDOW *win, chtype color){
	wattron(status_window, color);
	wprintw(status_window, text.c_str());
	wattroff(status_window, color);
}

static std::string makeStringLonger(std::string s, int length){
	if (s.size() > length){
		return s.substr(0, length);
	}
	for (int i = s.size()+1; i <= length; i++)
		s += " ";
	return s;
}

void repaintStatusWindow(){
	//s�tin default v��rtusi
	for (int i = 0; i < 3; i++){
		if (motor_cmd[i].empty()) motor_cmd[i] = "-";
		if (motor_port[i].empty()) motor_port[i] = "MISSING";
		if (motor_speed[i].empty()) motor_speed[i] = "-";

		motor_cmd[i] = makeStringLonger(motor_cmd[i], last_command_length);
		motor_port[i] = makeStringLonger(motor_port[i], 7);
		motor_speed[i] = makeStringLonger(motor_cmd[i], 5);

	}

	//teen stringid piisavalt pikaks, et kirjutada vana sodi �le
	coilgun_port = makeStringLonger(coilgun_port, 7);
	
	//prindin raami sisse pealkirjad
	wattron(status_window, COLOR_PAIR(3) | A_UNDERLINE);
	mvwprintw(status_window, 2+posy, 4+posx, "MOTORS   PORT      SPEED   LAST COMMAND");
	for (int i = 0; i < last_command_length - 12; i++) wprintw(status_window, " ");
	//mvwprintw(status_window, 2, 13, "PORT");
	//mvwprintw(status_window, 2, 20, "SPEED");
	//mvwprintw(status_window, 2, 28, "LAST COMMAND");
	for (int i = 0; i < 3; i++)
		mvwprintw(status_window, 3+i+posy, 4+posx, "ID%i    ", i);
	wattroff(status_window, COLOR_PAIR(3) | A_UNDERLINE);

	//prindin raami
	std::string frame = "----------------------------";
	for (int i = 0; i < last_command_length; i++) frame += "-";
	mvwprintw(status_window, 1 + posy, 2 + posx, ".%s.", frame.c_str());
	mvwprintw(status_window, 6 + posy, 2 + posx, "'%s'", frame.c_str());
	for (int y = 2; y < 6; y++)
		mvwprintw(status_window, y + posy, 2 + posx, "|");
	for (int y = 2; y < 6; y++)
		mvwprintw(status_window, y + posy, 31 + last_command_length + posx, "|");

	//kirjutan asjade v��rtused
	wattron(status_window, COLOR_PAIR(C_DEFAULT) | A_UNDERLINE);
	for (int i = 0; i < 3; i++){
		std::string port = motor_port[i];
		if (port == "MISSING") wattron(status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);
		mvwprintw(status_window, 3 + i + posy, 13 + posx, port.c_str());
		mvwprintw(status_window, 3 + i + posy, 23 + posx, motor_speed[i].c_str());
		mvwprintw(status_window, 3 + i + posy, 31 + posx, motor_cmd[i].c_str());
		if (port == "MISSING") wattroff(status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);
	}

	//kirjutan ridasid peale kasti
	mvwprintw(status_window, 8 + posy, 3 + posx, "Coilgun/dribbler: ");
	if (coilgun_port == "MISSING") writeTextWithColor(coilgun_port, status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);
	else writeTextWithColor(coilgun_port, status_window, COLOR_PAIR(C_DEFAULT));

	mvwprintw(status_window, 8 + posy, /*30*/getStatusContentWidth()-19 + posx, "Dribbler:    ");
	if (dribbler_state == 1) 
		writeTextWithColor("ON", status_window, COLOR_PAIR(C_GOOD));
	else if (dribbler_state == 0) 
		writeTextWithColor("OFF", status_window, COLOR_PAIR(C_WARNING));
	else 
		writeTextWithColor("UNKNOWN", status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);

	mvwprintw(status_window, 9 + posy, 3 + posx, "Ball in dribbler: ");
	if (ball_in_dribbler == 1) 
		writeTextWithColor("YES", status_window, COLOR_PAIR(C_GOOD));
	else if (ball_in_dribbler == 0) 
		writeTextWithColor("NO", status_window, COLOR_PAIR(C_WARNING));
	else 
		writeTextWithColor("UNKNOWN", status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);

	mvwprintw(status_window, 9 + posy, /*30*/getStatusContentWidth() - 19 + posx, "Balls found: ");
	if (balls_found == 0) 
		writeTextWithColor(std::to_string(balls_found), status_window, COLOR_PAIR(C_WARNING));
	else if (balls_found != -1) 
		writeTextWithColor(std::to_string(balls_found), status_window, COLOR_PAIR(C_GOOD));
	else 
		writeTextWithColor("NONE", status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);

	mvwprintw(status_window, 10 + posy, 3 + posx, "Last coilgun command: ");
	if (coilgun_port == "MISSING")
		writeTextWithColor(makeStringLonger(coilgun_cmd, last_command_length + 6), status_window, COLOR_PAIR(C_ERROR) | A_UNDERLINE);
	else if(coilgun_cmd == "-")
		writeTextWithColor(makeStringLonger(coilgun_cmd, last_command_length + 6), status_window, COLOR_PAIR(C_WARNING) | A_UNDERLINE);
	else
		writeTextWithColor(makeStringLonger(coilgun_cmd, last_command_length+6), status_window, COLOR_PAIR(C_GOOD));

	wrefresh(status_window);
}

//PUBLIC FUNCTIONS

//Functions for initializing and closing CUI
void cui_exit(){
	endwin();
}

void cui_init(std::string log_0_name, std::string log_1_name, std::string log_2_name, std::string status_name){
	initscr();
	resize_term(28, 128);
	noecho();		// kasutaja ei saa tr�kkida �le minu ilusa CUI
	curs_set(0);	// peidame kursori

	//S�tin v�rvipaarid valmis
	start_color();
	init_pair(0, COLOR_WHITE, COLOR_BLACK); //default
	init_pair(1, COLOR_BLACK, COLOR_YELLOW); //\n'ide n�itamiseks
	init_pair(2, COLOR_YELLOW, COLOR_BLACK); //logikorduste n�itamiseks
	init_color(COLOR_MAGENTA, 500, 500, 500);
	init_pair(3, COLOR_BLACK, COLOR_MAGENTA);//kastide v�rvimiseks hall v�rv
	init_pair(C_ERROR, COLOR_BLACK, COLOR_RED);	//errorite n�itamiseks
	init_pair(C_WARNING, COLOR_BLACK, COLOR_YELLOW);	//errorite n�itamiseks
	init_pair(C_GOOD, COLOR_BLACK, COLOR_GREEN);	//errorite n�itamiseks

	//Initialiseerin asju
	getmaxyx(stdscr, height, width);
	refresh(); //miskip�rsat on seda vaja, et aknad t��taksid

	//S�tin erinevad aknad valmis
	log_window[0] = create_newwin(height / 2, width / 2, 0, 0, log_0_name);
	log_window[1] = create_newwin(height / 2, width / 2, height / 2, 0, log_1_name);
	log_window[2] = create_newwin(height / 2, width / 2, height / 2, width / 2, log_2_name);
	status_window = create_newwin(height / 2, width / 2, 0, width / 2, status_name);

	//Staatuse stuff
	//paigutan staatuse sisu akna keskele
	posx = width  / 4 - getStatusContentWidth()  / 2 - 2;
	posy = height / 4 - getStatusContentHeight() / 2 - 1;

	repaintStatusWindow();
}

//For log output
void cui_write_to_log(int log_number, std::string text){
	if (logRepeatingTexts[log_number] == NULL){
		logRepeatingTexts[log_number] = 0;
	}

	if (logText[log_number].size() > 0 && text == logText[log_number].back()){
		//saadeti korduv tekst
		logRepeatingTexts[log_number]++;
	} else{
		//saadeti uus tekst
		if (logText[log_number].size() > 0 && logRepeatingTexts[log_number] > 0)
			logText[log_number].back() += " [" + std::to_string(logRepeatingTexts[log_number] + 1) + "]";
		logRepeatingTexts[log_number] = 0;
		logText[log_number].push_back(text);
	}
	updateLog(log_number);
}

//Staatuse s�ttimise funktsioonid
void cui_set_coilgun_port(std::string port){
	coilgun_port = port;
	repaintStatusWindow();
}

void cui_set_dribbler_state(int state){
	dribbler_state = state;
	repaintStatusWindow();
}

void cui_set_ball_in_dribbler(int is_ball_in_dribbler){
	ball_in_dribbler = is_ball_in_dribbler;
	repaintStatusWindow();
}

void cui_set_balls_found(int balls){
	balls_found = balls;
	repaintStatusWindow();
}

void cui_set_last_coilgun_command(std::string command){
	coilgun_cmd = command;
	repaintStatusWindow();
}

void cui_set_motor_port(int motor_id, std::string port){
	motor_port[motor_id] = port;
	repaintStatusWindow();
}

void cui_set_motor_speed(int motor_id, std::string speed){
	motor_speed[motor_id] = speed;
	repaintStatusWindow();
}

void cui_set_last_motor_command(int motor_id, std::string command){
	motor_cmd[motor_id] = command;
	repaintStatusWindow();
}


