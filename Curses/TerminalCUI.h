#ifndef _TERMINALGUI_H
#define _TERMINALGUI_H

#include "curses.h"
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>

/**
	Initializes the terminal window to display multiple logs simultaneously.
	@param log_0_name Defines the title for log #0
	@param log_1_name Defines the title for log #1
	@param log_2_name Defines the title for log #2
	@param status_name Defines the title for the status window
*/
void cui_init(std::string log_0_name, std::string log_1_name, std::string log_2_name, std::string status_name);

/**
	Writes some text to log
	@param log_number A number [0;1;2] that corrasponds to the log that text should be appended to
	@param text The text that should be appended to the and of log
*/
void cui_write_to_log(int log_number, std::string text);

/**
	Sets the port for coilgun in the status window
	@param port A string representation of the port that coilgun is connected to. Defaults to "MISSING"
*/
void cui_set_coilgun_port(std::string port = "MISSING");

/**
	Sets the state of dribbler in the status window
	@param state A number [-1;0;1] that shows if dribbler is on [1], off [0] or not found/not connected [-1]. Defaults to not connected/not found [-1]
*/
void cui_set_dribbler_state(int state = -1);

/**
	Sets the status of ball being in dribbler in the status window
	@param is_ball_in_dribbler A number [-1;0;1] that shows if a ball is in dribbler. A value of [1] means that a ball is in dribbler, [0] means that no ball is currently in dribbler and [-1] means that the status of ball being in dribbler is unknown. Defaults to [-1]
*/
void cui_set_ball_in_dribbler(int is_ball_in_dribbler = -1);

/**
	Sets the number of balls found in the status window
	@param balls A number representing the number of balls found. A value of [-1] means that the number of found balls is unknown. Defaults to [-1]
*/
void cui_set_balls_found(int balls = -1);

/**
	Sets the last command sent to coilgun in the status window
	@param command Last command that was sent to coilgun. Defaults to "-"
*/
void cui_set_last_coilgun_command(std::string command = "-");

/**
	Sets the port a motor is connected to in the status window
	@param motor_id A number [0;1;2] representing the ID of the motor
	@param port A string representing the port the motor is connected to. Defaults to "MISSING"
*/
void cui_set_motor_port(int motor_id, std::string port = "MISSING");

/**
	Sets the desired speed of a motor in the status window
	@param motor_id A number [0;1;2] representing the ID of the motor
	@param speed A string representing the desired speed of a motor. Defaults to "-"
*/
void cui_set_motor_speed(int motor_id, std::string speed = "-");

/**
	Sets the last command sent to a motor in the status window
	@param motor_id A number [0;1;2] representing the ID of the motor
	@param command Last command that was sent to this motor. Defaults to "-"
*/
void cui_set_last_motor_command(int motor_id, std::string command = "-");

/**
	Closes the cui, makes printf-s and other standard output functions work again. Needs to be called before program exits.
*/
void cui_exit();

#endif